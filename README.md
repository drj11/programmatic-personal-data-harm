# programmatic-personal-data-harm

A collection of notes on where
programmatic processing of personal data causes harm.

Starting with Facebook.


## Actions

2018-12 Washington DC sue Facebook, accusing it of "allowing the
wholesale scraping of personal data on tens of millions of
users". https://www.bbc.co.uk/news/technology-46627133

2018-12 New York Times reveals how Facebook shares personal data
(example: PMs, friend's activities) with third party companies
(for example, Spotify).
https://www.bbc.co.uk/news/technology-46618582

2018-10 ICO issues notice to Aggregate IQ for failing to comply
with current GDPR legislation
https://ico.org.uk/action-weve-taken/enforcement/aggregate-iq-data-services-ltd/

2018-10 Irish Data Protection Commission opens investigation
into data breach affecting 50 million people.

2018-07 University of Cambridge Psychometrics Centre audited by ICO
after it harvested and exported to Cambridge Analytica
the data of 87 million individuals from Facebook.
https://tech.newstatesman.com/gdpr/cambridge-university-psychometrics-centre-gdpr-ico
[published 2018-11:
https://ico.org.uk/action-weve-taken/audits-advisory-visits-and-overview-reports/cambridge-university-psychometrics-centre/]

2018-07 Facebook investigated by SEC.
Abouts its "massive data leak to Cambridge Analytica".
Facebook are already under investigation by the FBI,
the Justice Department, and the (US) Federal Trade Commission. 
https://www.ft.com/content/36fea01e-7e4e-11e8-bc55-50daf11b720d

2018-07 Facebook fined GBP 500e3 by ICO
https://www.bbc.co.uk/news/technology-44785151
https://ico.org.uk/about-the-ico/news-and-events/news-and-blogs/2018/07/findings-recommendations-and-actions-from-ico-investigation-into-data-analytics-in-political-campaigns/

2017-05 Facebook fined by EU Competition Commission
for misleading claims when purchasing WhatsApp.

I found this ICO executive summary document, but I can't find
out where. https://ico.org.uk/media/action-weve-taken/2260270/executive-summary.pdf It's pretty good.
ICO are now pursuing a criminal case against Cambridge Analytica.



## Stories

The ICO investigation: "concluded that Facebook contravened the law
by failing to safeguard people’s information. It also found that the
company failed to be transparent about how people’s data was harvested
by others."

Bill Hart-Davidson's post is useful and thoughtful.
It deflects questions away from the self and directs them
outward.
In discussing the harm caused by sharing
a "now versus 10 years ago" photo meme,
they point out that the harm to self is small or negligible,
but that what is happening is that a better system is being built:
a system capable of great harm
(mass facial recognition even with old pictures).

Hilary Mason (who has a wikipedia page,
https://en.wikipedia.org/wiki/Hilary_Mason_(data_scientist))
said on Twitter:

"""
One company collects sensitive data.
It's a big, public, company.
We trust they won't do anything too bad.

But they sell the data,
and there's no legal requirement to disclose when they do.

...and you shouldn't worry, because those customers agree to legal ToS.
But those ToS are never enforced.
And they also sell their data.
And don't disclose it.
...and inevitably, it's in the hands of people who will use it for awful things.
"""

So we see this pattern of companies using Terms of Service as a
way to put at arm's length the harm that is done using data
that they collect.

Facebook is negligent to the harm it enables.
Example: 300e3 install app;
app collects data on 87e6 people.
Facebook's defence,
when faced with stories of data exfiltration and so on,
is that "the user's consented".
But this is a false defence,
the users are not in a position to be fully informed and
give consent freely.


## Critiques

### Psychometric Centre

The ICO's audit of the Unversity of Cambridge Psychometric
Centre is here, including its summary report: https://ico.org.uk/action-weve-taken/audits-advisory-visits-and-overview-reports/cambridge-university-psychometrics-centre/

It finds that at the university level there are improvements to
be made, and that the current policies are outdated and
requiring updating to modern legislation.

Many recommendations are made
(only tabulated, not detailed, in this report), including 21
recommendations in the area of Security of Personal Data, 6 of
which are Urgent.
The report does not detail these recommendations.


### ICO Commissioner

In this enforcement notice against AIQ the commissioner "takes
the view that damage or distress is likely as a result of data
subjects being denied the opportunity of properly understanding
what personal data may be processed about them by the
controller".
Showing that it is personal data (of unknown nature) being
processed beyond their control and understanding that causes
damage.

ICO say that Universities UK have set up a working group to
consider the wider privacy and ethical implications  of using
social media data in research. [but I can't find it; their later
report (see below) says that Universities UK "has committed to
do so", and "will convene a working group"]

ICO's wrap-up report https://ico.org.uk/media/action-weve-taken/2260271/investigation-into-the-use-of-data-analytics-in-political-campaigns-final-20181105.pdf "has identified 
some common and potentially serious data protection concerns that we 
suspect are widespread across the university sector"

"What is clear is a serious overhaul of data protection practices is
needed i n how higher education institutions handle data in the context
of academic research"

The wrap-up report is a good synthesis of ICO's activities in
this area.

## ad hoc

In
https://www.politico.eu/article/inside-story-facebook-fight-against-european-regulation/
we find: "Facebook has consistently been tone-deaf about major concerns
brought to their attention" — Marietje Schaake, Dutch liberal MEP

Facebook consistently resists (and defies, see fines above) EU regulation,
and promotes its own platforms and software that
limit citizen's speech in arbitrary ways.


## REFERENCES

Andre Staltz decodes some of Facebooks "presumptive helpful"
posturing:
https://mobile.twitter.com/andrestaltz/status/1084947956548149248

Ondatra iSchoolicus
"A DATA SET, ONCE GATHERED, CAN BE HARD TO
DESTROY IF SOMEBODY WANTS IT FOR SOCIAL CONTROL OR TO MAKE $$$."
https://mobile.twitter.com/LibSkrat/status/1084930285211394048

Bill Hart-Davidson
https://medium.com/@billhd/sharing-in-the-age-of-platforms-and-machine-learning-public-data-vs-630d4bd26b30

Hilary Mason
https://twitter.com/hmason/status/1082708805094273024
